console.log("wew");

class Student {
  constructor(name, email, grades){
    this.name = name;
    this.email = email;

    // check if first of the array has 4 elements
    if(grades.length === 4){
      if(grades.every(grade => grade >= 0 && grade <= 100)){
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }

    this.gradeAve = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;
  }
  // class methods - would be common to all instances.
  // make sure they are NOT separated by a comma
  login(){
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout(email){
    console.log(`${email} has logged out`);
    return this;
  }
  listGrades(grades){
    this.grades.forEach(grade => {
      console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    })
    return this;
  }
  computeAve(){
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    this.gradeAve = sum / 4;
    return this;
  }
  willPass(){
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }
  willPassWithHonors(){
    if(this.passed){
      if(this.gradeAve >= 90){
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
 
}

class Section {
  constructor(name){
    // every instantiated Section object will have an empty array for its students
    this.name = name;
    this.students = [];
    this.honorStudents = undefined;
  }

  // method for adding student to this section
  // this take in the same arguments needed to instantiate a Student object
  addStudent(name, email, grades){
    // a Student object will be instantiated before being pushed to the students property
    this.students.push(new Student(name, email, grades));
    // return the Section object afterwards, allowing us to chain this method
    return this;
  }
  // method for computing how many students in the section are honor students
  countHonorStudents(){
    let count = 0;
    // remember that each student here is an object instantiated from the Student class
    this.students.forEach(student => {
      if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
        count++
      }
    })
    this.honorStudents = count;
    return this;
  }

  computeHonorsPercentage(){
    this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
    return this;
  }
  computeSectionAve(){
    let sum = 0;
    this.students.forEach(student => {
      student.grades.forEach(grade => sum = sum + grade);
      this.gradeAve = sum / 4;
    })
    console.log(sum/4);
    return this;
  }
}
// instantiate a Section object
// const section1A = new Section("section1A");
// console.log(section1A);



// let studentOne = section1A.addStudent("Tony", "starkisundustries@mail.com", [89, 84, 78, 88]);
// let studentTwo = section1A.addStudent("Peter", "spidey@mail.com", [70, 70, 70, 70]);
// let studentThree = section1A.addStudent("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
// let studentFour = section1A.addStudent("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);


class Grade {
  constructor(level){
    this.level = level;
    this.sections = [];
    this.totalStudents = 0;
    this.totalHonorStudents = 0;
    this.batchAveGrade = undefined;
    this.batchMinGrade = undefined;
    this.batchMaxGrade = undefined;
  }
  addSection(name){
    this.sections.push(new Section(name));
    return this;
  }
  countStudents(){
    this.sections.forEach(section => this.totalStudents = this.totalStudents + section.students.length);
    return this;
  }
  countHonorStudents(){
    let count = 0;
    this.sections.forEach(section => {
      section.students.forEach(student => {
        if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
          count++
        }
      })
    })
    this.totalHonorStudents = count;
    return this;
  }
  computeBatchAve(){
    let sum = 0;
    let studentCount = 0;
    this.sections.forEach(section => {
      section.students.forEach(student => {
        sum = sum + student.computeAve().gradeAve;
        studentCount++;
        // console.log(studentCount);
      })
    })
    this.batchAveGrade = Math.round(sum / studentCount)
    return this;
  }
  getBatchMinGrade(){
    let min;
    this.sections.forEach(section => {
      section.students.forEach(student => {
        student.grades.forEach(grade => {
          console.log(grade);
          if(typeof min === 'undefined' || grade < min){
            min = grade;
          }
        })
      })
    })
    this.batchMinGrade = min;
    return this;

  }
  getBatchMaxGrade(){
    let max;
    this.sections.forEach(section => {
      section.students.forEach(student => {
        student.grades.forEach(grade => {
          console.log(grade)
          if(typeof max === 'undefined' || grade > max);
          max = grade;
        })
      })
    })
    this.batchMaxGrade = max;
    return this;

  }
}
const grade1 = new Grade(1);
console.log(grade1);

// populate the grade level with sections
grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");

const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

// populate the sections with students
section1A.addStudent("Tony", "starkisundustries@mail.com", [89, 84, 78, 88]);
section1A.addStudent("Peter", "spidey@mail.com", [70, 70, 70, 70]);
section1A.addStudent("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
section1A.addStudent("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);



